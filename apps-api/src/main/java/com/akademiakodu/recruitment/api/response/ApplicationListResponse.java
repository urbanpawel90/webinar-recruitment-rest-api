package com.akademiakodu.recruitment.api.response;

import com.akademiakodu.recruitment.api.entity.ApplicationHeader;

import java.util.List;

public class ApplicationListResponse {
    private List<ApplicationHeader> application;

    public ApplicationListResponse() {
    }

    public ApplicationListResponse(List<ApplicationHeader> application) {
        this.application = application;
    }

    public List<ApplicationHeader> getApplication() {
        return application;
    }

    public void setApplication(List<ApplicationHeader> application) {
        this.application = application;
    }
}
