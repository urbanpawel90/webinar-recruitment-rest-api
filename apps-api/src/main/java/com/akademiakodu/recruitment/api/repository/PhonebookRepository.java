package com.akademiakodu.recruitment.api.repository;

import com.akademiakodu.recruitment.api.entity.PhonebookEntry;

import java.util.List;
import java.util.UUID;

public interface PhonebookRepository {
    List<PhonebookEntry> findAll();

    UUID create(PhonebookEntry entry);

    void update(PhonebookEntry entry);

    void delete(UUID id);
}
