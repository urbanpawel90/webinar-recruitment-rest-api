package com.akademiakodu.recruitment.api.entity;

public class Rating {
    private String name;
    private Integer rate;

    public Rating() {
    }

    public Rating(String name, int rate) {
        this.name = name;
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }
}
