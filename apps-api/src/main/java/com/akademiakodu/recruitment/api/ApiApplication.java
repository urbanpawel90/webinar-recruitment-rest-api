package com.akademiakodu.recruitment.api;

import com.akademiakodu.recruitment.api.configuration.ImagesConfiguration;
import com.akademiakodu.recruitment.api.configuration.RepositoryConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({
        ImagesConfiguration.class,
        RepositoryConfiguration.class
})
public class ApiApplication {
    public static final String JSON_CONTENT_TYPE = "application/json";

    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }
}
