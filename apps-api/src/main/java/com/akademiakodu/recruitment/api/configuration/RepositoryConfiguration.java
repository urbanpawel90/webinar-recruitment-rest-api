package com.akademiakodu.recruitment.api.configuration;

import com.akademiakodu.recruitment.api.entity.ApplicationDetails;
import com.akademiakodu.recruitment.api.entity.PhonebookEntry;
import com.akademiakodu.recruitment.api.repository.ApplicationRepository;
import com.akademiakodu.recruitment.api.repository.InMemoryApplicationRepository;
import com.akademiakodu.recruitment.api.repository.InMemoryPhonebookRepository;
import com.akademiakodu.recruitment.api.repository.PhonebookRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Configuration
public class RepositoryConfiguration {
    @Bean
    public ApplicationRepository applicationRepository(ObjectMapper mapper) throws IOException {
        ClassLoader cl = this.getClass().getClassLoader();
        InputStream inputStream = cl.getResourceAsStream("initial.json");
        List<ApplicationDetails> details = mapper.readValue(inputStream,
                new TypeReference<List<ApplicationDetails>>() {
                });
        return new InMemoryApplicationRepository(details);
    }

    @Bean
    public PhonebookRepository phonebookRepository() {
        List<PhonebookEntry> exampleData = new ArrayList<>();
        PhonebookEntry entry = new PhonebookEntry();
        entry.setId(UUID.randomUUID());
        entry.setEmail("urbanpawel90@gmail.com");
        entry.setName("Paweł Urban");
        entry.setNumber("+48 666 19 69 00");
        exampleData.add(entry);

        return new InMemoryPhonebookRepository(exampleData);
    }
}
