package com.akademiakodu.recruitment.api.repository;

import com.akademiakodu.recruitment.api.entity.ApplicationDetails;
import com.akademiakodu.recruitment.api.entity.ApplicationHeader;

import java.util.List;

public interface ApplicationRepository {
    List<ApplicationHeader> getHeaders();

    ApplicationDetails getApplicationDetail(int id);
}
