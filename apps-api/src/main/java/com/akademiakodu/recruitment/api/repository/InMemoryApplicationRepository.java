package com.akademiakodu.recruitment.api.repository;

import com.akademiakodu.recruitment.api.entity.ApplicationDetails;
import com.akademiakodu.recruitment.api.entity.ApplicationHeader;

import java.util.List;
import java.util.stream.Collectors;

public class InMemoryApplicationRepository implements ApplicationRepository {
    private List<ApplicationDetails> records;

    public InMemoryApplicationRepository(List<ApplicationDetails> records) {
        if (records == null || records.size() == 0) {
            throw new NullPointerException("In-memory repository records can't be null and empty !");
        }
        this.records = records;
    }

    @Override
    public List<ApplicationHeader> getHeaders() {
        return records.stream()
                .map(this::getHeaderFromDetails)
                .collect(Collectors.toList());
    }

    private ApplicationHeader getHeaderFromDetails(ApplicationDetails detail) {
        ApplicationHeader header = new ApplicationHeader();
        header.setId(detail.getId());
        header.setImage(detail.getIcon());
        header.setTitle(detail.getName());
        return header;
    }

    @Override
    public ApplicationDetails getApplicationDetail(int id) {
        for (ApplicationDetails detail : this.records) {
            if (detail.getId() == id) {
                return detail;
            }
        }
        return null;
    }
}
