package com.akademiakodu.recruitment.api.endpoint;

import com.akademiakodu.recruitment.api.entity.PhonebookEntry;
import com.akademiakodu.recruitment.api.repository.PhonebookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.akademiakodu.recruitment.api.ApiApplication.JSON_CONTENT_TYPE;

@RestController
@RequestMapping(value = "/phonebook", produces = JSON_CONTENT_TYPE)
public class PhonebookEndpoint {
    @Autowired
    private PhonebookRepository mPhonebookRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<PhonebookEntry> getPhonebook() {
        return mPhonebookRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.PUT)
    public PhonebookEntry createEntry(@RequestBody PhonebookEntry entry) {
        UUID id = mPhonebookRepository.create(entry);
        entry.setId(id);

        return entry;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}")
    public PhonebookEntry updateEntry(@PathVariable("id") UUID id, @RequestBody PhonebookEntry entry) {
        entry.setId(id);
        mPhonebookRepository.update(entry);

        return entry;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity deleteEntry(@PathVariable("id") UUID id) {
        mPhonebookRepository.delete(id);

        return ResponseEntity.ok(null);
    }
}
