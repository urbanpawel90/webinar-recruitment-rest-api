package com.akademiakodu.recruitment.api.repository;

import com.akademiakodu.recruitment.api.entity.PhonebookEntry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class InMemoryPhonebookRepository implements PhonebookRepository {
    private final List<PhonebookEntry> mDatabase;

    public InMemoryPhonebookRepository(List<PhonebookEntry> mDatabase) {
        this.mDatabase = Collections.synchronizedList(new ArrayList<>(mDatabase));
    }

    @Override
    public List<PhonebookEntry> findAll() {
        return Collections.unmodifiableList(mDatabase);
    }

    @Override
    public UUID create(PhonebookEntry entry) {
        UUID newId;
        do {
            newId = UUID.randomUUID();
        } while (getEntry(newId) != null);

        entry.setId(newId);
        mDatabase.add(entry);

        return newId;
    }

    private PhonebookEntry getEntry(UUID newId) {
        for (PhonebookEntry entry : mDatabase) {
            if (entry.getId().equals(newId)) {
                return entry;
            }
        }
        return null;
    }

    @Override
    public void update(PhonebookEntry entry) {
        PhonebookEntry existing = getEntry(entry.getId());
        if (existing == null) return;

        mDatabase.remove(existing);
        mDatabase.add(entry);
    }

    @Override
    public void delete(UUID id) {
        PhonebookEntry existing = getEntry(id);
        if (existing == null) return;

        mDatabase.remove(existing);
    }
}
