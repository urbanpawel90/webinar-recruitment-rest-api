package com.akademiakodu.recruitment.api.entity;

public class AppStoreLinks {
    private String android;
    private String ios;

    public AppStoreLinks() {
    }

    public AppStoreLinks(String android, String ios) {
        this.android = android;
        this.ios = ios;
    }

    public String getAndroid() {
        return android;
    }

    public void setAndroid(String android) {
        this.android = android;
    }

    public String getIos() {
        return ios;
    }

    public void setIos(String ios) {
        this.ios = ios;
    }
}
