package com.akademiakodu.recruitment.api.exception;

import java.util.ArrayList;
import java.util.List;

public class RatingValidationException extends Exception {
    private Errors errors;

    public RatingValidationException(Errors errors) {
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }

    public static class Errors {
        private List<String> name = new ArrayList<>();
        private List<String> rating = new ArrayList<>();

        public Errors() {
        }

        public boolean hasErrors() {
            return (name.size() + rating.size()) > 0;
        }

        public void addNameError(String nameError) {
            name.add(nameError);
        }

        public void addRatingError(String ratingError) {
            rating.add(ratingError);
        }

        public List<String> getName() {
            return name;
        }

        public void setName(List<String> name) {
            this.name = name;
        }

        public List<String> getRating() {
            return rating;
        }

        public void setRating(List<String> rating) {
            this.rating = rating;
        }
    }
}
