package com.akademiakodu.recruitment.api.entity;

import java.util.List;

public class ApplicationDetails {
    private int id;
    private String name;
    private String description;
    private String icon;
    private List<String> gallery;
    private AppStoreLinks links;
    private List<Rating> rating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public AppStoreLinks getLinks() {
        return links;
    }

    public void setLinks(AppStoreLinks links) {
        this.links = links;
    }

    public List<Rating> getRating() {
        return rating;
    }

    public void setRating(List<Rating> rating) {
        this.rating = rating;
    }
}
