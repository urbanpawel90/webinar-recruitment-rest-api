package com.akademiakodu.recruitment.api.endpoint;

import com.akademiakodu.recruitment.api.entity.ApplicationDetails;
import com.akademiakodu.recruitment.api.entity.Rating;
import com.akademiakodu.recruitment.api.exception.RatingValidationException;
import com.akademiakodu.recruitment.api.repository.ApplicationRepository;
import com.akademiakodu.recruitment.api.response.ApplicationListResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.akademiakodu.recruitment.api.ApiApplication.JSON_CONTENT_TYPE;

@RestController
@RequestMapping(value = "/applications", produces = JSON_CONTENT_TYPE)
public class ApplicationsEndpoint {
    @Autowired
    private ApplicationRepository applicationRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ApplicationListResponse getApplications() {
        return new ApplicationListResponse(applicationRepository.getHeaders());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ApplicationDetails getApplicationDetails(@PathVariable("id") int id) {
        return applicationRepository.getApplicationDetail(id);
    }

    @RequestMapping(value = "/{id}/ratings", method = RequestMethod.POST, consumes = JSON_CONTENT_TYPE)
    public ResponseEntity addRating(@RequestBody Rating rating, @PathVariable("id") int id)
            throws RatingValidationException {
        validateInput(rating);
        getApplicationDetails(id).getRating().add(rating);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    private void validateInput(Rating rating) throws RatingValidationException {
        RatingValidationException.Errors errors = new RatingValidationException.Errors();
        if (rating.getName() == null || rating.getName().trim().isEmpty()) {
            errors.addNameError("Nazwa nie może być pusta !");
        }
        if (rating.getRate() == null || rating.getRate() < 1 || rating.getRate() > 5) {
            errors.addRatingError("Ocena musi być z zakresu 1-5 !");
        }

        if (errors.hasErrors()) {
            throw new RatingValidationException(errors);
        }
    }

    @ExceptionHandler(RatingValidationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public RatingValidationException.Errors onRatingValidationException(RatingValidationException exception) {
        return exception.getErrors();
    }
}
